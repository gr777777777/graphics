
// TestView.h : CTestView 类的接口
//

#pragma once

#include "P2.h"//包含点类
class CTestView : public CView
{
protected: // 仅从序列化创建
	CTestView();
	DECLARE_DYNCREATE(CTestView)

// 特性
public:
	CTestDoc* GetDocument() const;

// 操作
public:

// 重写
public:
	void DoubleBuffer(CDC *pDC);//双缓冲
	void DrawRect(CDC *pDC,int nscale);//绘制放大镜窗口
	CP2  Convert(CPoint point);//坐标系转换
	void Diamond(CDC *pDC,BOOL bclip);//绘制金刚石图案
	int	 ZoomX(int x);//X坐标增减函数
	int  ZoomY(int y);//Y坐标增减函数
	BOOL LBLineClip();//裁剪函数
	BOOL ClipTest(double,double ,double &,double &);//裁剪测试函数
	virtual void OnDraw(CDC* pDC);  // 重写以绘制该视图
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 实现
public:
	virtual ~CTestView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	CP2  P[2];//直线的起点和终点
	CP2  *V;//动态定义等分点数组
	UINT nScale;//放大镜比例
	CP2 nRCenter;//裁剪矩形的中心坐标
	double nRHHeight,nRHWidth;//裁剪矩形的半高度和半宽度
// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()
public:
//	afx_msg void OnDrawpic();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
//	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
//	afx_msg void OnClip();
//	afx_msg void OnUpdateClip(CCmdUI *pCmdUI);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnZoomin();
	afx_msg void OnZoomout();
};

#ifndef _DEBUG  // TestView.cpp 中的调试版本
inline CTestDoc* CTestView::GetDocument() const
   { return reinterpret_cast<CTestDoc*>(m_pDocument); }
#endif

