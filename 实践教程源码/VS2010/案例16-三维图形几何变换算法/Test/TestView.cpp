
// TestView.cpp : CTestView 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "Test.h"
#endif

#include "TestDoc.h"
#include "TestView.h"
#include "math.h"//包含数学头文件
#define  PI 3.1415926//PI的宏定义
#define Round(d) int(floor(d+0.5))//四舍五入宏定义

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CTestView

IMPLEMENT_DYNCREATE(CTestView, CView)

BEGIN_MESSAGE_MAP(CTestView, CView)
	// 标准打印命令
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_COMMAND(IDM_DRAWPIC, &CTestView::OnDrawpic)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_COMMAND(ID_TBACK, &CTestView::OnTback)
	ON_COMMAND(ID_TDOWN, &CTestView::OnTdown)
	ON_COMMAND(ID_TFRONT, &CTestView::OnTfront)
	ON_COMMAND(ID_TLEFT, &CTestView::OnTleft)
	ON_COMMAND(ID_TRIGHT, &CTestView::OnTright)
	ON_COMMAND(ID_TUP, &CTestView::OnTup)
	ON_COMMAND(ID_SDECREASE, &CTestView::OnSdecrease)
	ON_COMMAND(ID_SINCREASE, &CTestView::OnSincrease)
	ON_COMMAND(ID_SXDIRECTIONPLUS, &CTestView::OnSxdirectionplus)
	ON_COMMAND(ID_SYDIRECTIONPLUS, &CTestView::OnSydirectionplus)
	ON_COMMAND(ID_SZDIRECTIONNEG, &CTestView::OnSzdirectionneg)
	ON_COMMAND(ID_RESET, &CTestView::OnReset)
	ON_COMMAND(ID_RXAXIS, &CTestView::OnRxaxis)
	ON_COMMAND(ID_RXOY, &CTestView::OnRxoy)
	ON_COMMAND(ID_RXOZ, &CTestView::OnRxoz)
	ON_COMMAND(ID_RYAXIS, &CTestView::OnRyaxis)
	ON_COMMAND(ID_RYOZ, &CTestView::OnRyoz)
	ON_COMMAND(ID_RZAXIS, &CTestView::OnRzaxis)
END_MESSAGE_MAP()

// CTestView 构造/析构

CTestView::CTestView()
{
	// TODO: 在此处添加构造代码

}

CTestView::~CTestView()
{
}

BOOL CTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CView::PreCreateWindow(cs);
}

// CTestView 绘制

void CTestView::OnDraw(CDC* pDC)
{
	CTestDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 在此处为本机数据添加绘制代码
	DoubleBuffer(pDC);
}


// CTestView 打印

BOOL CTestView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 默认准备
	return DoPreparePrinting(pInfo);
}

void CTestView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加额外的打印前进行的初始化过程
}

void CTestView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加打印后进行的清理过程
}


// CTestView 诊断

#ifdef _DEBUG
void CTestView::AssertValid() const
{
	CView::AssertValid();
}

void CTestView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTestDoc* CTestView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTestDoc)));
	return (CTestDoc*)m_pDocument;
}
#endif //_DEBUG


// CTestView 消息处理程序


void CTestView::OnDrawpic() 
{
	// TODO: Add your command handler code here
	MessageBox(CString("请使用图标按钮进行三维几何变换"),CString("提示"),MB_ICONEXCLAMATION|MB_OK);
}

void CTestView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	CView::OnLButtonDown(nFlags, point);
}


void CTestView::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	
	CView::OnLButtonUp(nFlags, point);
}
void CTestView::ReadPoint()//点表
{
	double a=100;//立方体边长为a
	//顶点的三维坐标(x,y,z)
	P[0].x=0;P[0].y=0;P[0].z=0;
	P[1].x=a;P[1].y=0;P[1].z=0;
	P[2].x=a;P[2].y=a;P[2].z=0;
	P[3].x=0;P[3].y=a;P[3].z=0;
	P[4].x=0;P[4].y=0;P[4].z=a;
	P[5].x=a;P[5].y=0;P[5].z=a;
	P[6].x=a;P[6].y=a;P[6].z=a;
	P[7].x=0;P[7].y=a;P[7].z=a;
}

void CTestView::ReadFace()//面表
{
	//面的边数、面的顶点编号
	F[0].SetNum(4);F[0].vI[0]=4;F[0].vI[1]=5;F[0].vI[2]=6;F[0].vI[3]=7;//前面
	F[1].SetNum(4);F[1].vI[0]=0;F[1].vI[1]=3;F[1].vI[2]=2;F[1].vI[3]=1;//后面
	F[2].SetNum(4);F[2].vI[0]=0;F[2].vI[1]=4;F[2].vI[2]=7;F[2].vI[3]=3;//左面
	F[3].SetNum(4);F[3].vI[0]=1;F[3].vI[1]=2;F[3].vI[2]=6;F[3].vI[3]=5;//右面
	F[4].SetNum(4);F[4].vI[0]=2;F[4].vI[1]=3;F[4].vI[2]=7;F[4].vI[3]=6;//顶面
	F[5].SetNum(4);F[5].vI[0]=0;F[5].vI[1]=1;F[5].vI[2]=5;F[5].vI[3]=4;//底面
}

void CTestView::DoubleBuffer(CDC *pDC)//双缓冲
{
	CRect rect;//定义客户区矩形
	GetClientRect(&rect);//获得客户区的大小
	pDC->SetMapMode(MM_ANISOTROPIC);//pDC自定义坐标系
	pDC->SetWindowExt(rect.Width(),rect.Height());//设置窗口范围
	pDC->SetViewportExt(rect.Width(),-rect.Height());//设置视区范围,x轴水平向右，y轴垂直向上
	pDC->SetViewportOrg(rect.Width()/2,rect.Height()/2);//客户区中心为原点
	CDC memDC;//内存DC
	CBitmap NewBitmap,*pOldBitmap;//内存中承载的临时位图
	memDC.CreateCompatibleDC(pDC);//创建一个与显示pDC兼容的内存memDC 
	NewBitmap.CreateCompatibleBitmap(pDC,rect.Width(),rect.Height());//创建兼容位图 
	pOldBitmap=memDC.SelectObject(&NewBitmap);//将兼容位图选入memDC 
	memDC.FillSolidRect(rect,pDC->GetBkColor());//按原来背景填充客户区，否则是黑色
	memDC.SetMapMode(MM_ANISOTROPIC);//memDC自定义坐标系
	memDC.SetWindowExt(rect.Width(),rect.Height());
	memDC.SetViewportExt(rect.Width(),-rect.Height());
	memDC.SetViewportOrg(rect.Width()/2,rect.Height()/2);
	rect.OffsetRect(-rect.Width()/2,-rect.Height()/2);
	DrawObject(&memDC);//向memDC绘制图形
	pDC->BitBlt(rect.left,rect.top,rect.Width(),rect.Height(),&memDC,-rect.Width()/2,-rect.Height()/2,SRCCOPY);//将内存memDC中的位图拷贝到显示pDC中
	memDC.SelectObject(pOldBitmap);//恢复位图
	NewBitmap.DeleteObject();//删除位图
}

void CTestView::ObliqueProject(CP3 p)//斜等测变换
{
	ScreenP.x=p.x-p.z/sqrt(2.0);
	ScreenP.y=p.y-p.z/sqrt(2.0);	
}

void CTestView::DrawObject(CDC *pDC)//绘制图形
{
	CLine *line=new CLine;//绘制坐标系
	line->MoveTo(pDC,0,0);//绘制x轴
	line->LineTo(pDC,rect.Width()/2,0);
	pDC->TextOut(rect.Width()/2-20,-20,CString("x"));
	line->MoveTo(pDC,0,0);//绘制y轴
	line->LineTo(pDC,0,rect.Height()/2);
	pDC->TextOut(-20,rect.Height()/2-20,CString("y"));
	line->MoveTo(pDC,0,0);//绘制z轴
	line->LineTo(pDC,-rect.Width()/2,-rect.Width()/2);
	pDC->TextOut(-rect.Height()/2-20,-rect.Height()/2+20,CString("z"));
	pDC->TextOut(10,-10,CString("O"));
	DrawPolygon(pDC,line);
	delete line;
}

void CTestView::DrawPolygon(CDC *pDC,CLine *line)//绘制立方体线框模型
{
	for(int nFace=0;nFace<6;nFace++)
	{
		CP2 t;	
		for(int nVertex=0;nVertex<F[nFace].vN;nVertex++)//顶点循环
		{
			ObliqueProject(P[F[nFace].vI[nVertex]]);//斜等测投影
			if(0==nVertex)
			{
				line->MoveTo(pDC,ScreenP.x,ScreenP.y);
				t=ScreenP;
			}
			else
				line->LineTo(pDC,ScreenP.x,ScreenP.y);
		}
		line->LineTo(pDC,t.x,t.y);//闭合多边形
	}
}

void CTestView::OnInitialUpdate()
{
	CView::OnInitialUpdate();

	// TODO: 在此添加专用代码和/或调用基类
	ReadPoint();
	ReadFace();
	trans.SetMat(P,8);
}


void CTestView::OnTback()
{
	// TODO: 在此添加命令处理程序代码
	trans.Translate(0,0,-10);
	Invalidate(FALSE);	
}


void CTestView::OnTdown()
{
	// TODO: 在此添加命令处理程序代码
	trans.Translate(0,-10,0);
	Invalidate(FALSE);
}


void CTestView::OnTfront()
{
	// TODO: 在此添加命令处理程序代码
	trans.Translate(0,0,10);
	Invalidate(FALSE);
}


void CTestView::OnTleft()
{
	// TODO: 在此添加命令处理程序代码
	trans.Translate(-10,0,0);
	Invalidate(FALSE);
}


void CTestView::OnTright()
{
	// TODO: 在此添加命令处理程序代码
	CDC *pDC=GetDC();
	trans.Translate(10,0,0);
	DoubleBuffer(pDC);
	ReleaseDC(pDC);
}


void CTestView::OnTup()
{
	// TODO: 在此添加命令处理程序代码
	trans.Translate(0,10,0);
	Invalidate(FALSE);
}


void CTestView::OnSdecrease()
{
	// TODO: 在此添加命令处理程序代码
	trans.Scale(0.5,0.5,0.5);
	Invalidate(FALSE);	
}


void CTestView::OnSincrease()
{
	// TODO: 在此添加命令处理程序代码
	trans.Scale(2,2,2);
	Invalidate(FALSE);
}


void CTestView::OnSxdirectionplus()
{
	// TODO: 在此添加命令处理程序代码
	trans.ShearX(1,1);
	Invalidate(FALSE);
}


void CTestView::OnSydirectionplus()
{
	// TODO: 在此添加命令处理程序代码
	trans.ShearY(1,1);
	Invalidate(FALSE);	
}


void CTestView::OnSzdirectionneg()
{
	// TODO: 在此添加命令处理程序代码
	trans.ShearZ(1,1);
	Invalidate(FALSE);
}


void CTestView::OnReset()
{
	// TODO: 在此添加命令处理程序代码
	ReadPoint();
	Invalidate(FALSE);
}


void CTestView::OnRxaxis()
{
	// TODO: 在此添加命令处理程序代码
	trans.RotateX(30,P[0]);
	Invalidate(FALSE);
}


void CTestView::OnRxoy()
{
	// TODO: 在此添加命令处理程序代码
	trans.ReflectXOY();
	Invalidate(FALSE);
}


void CTestView::OnRxoz()
{
	// TODO: 在此添加命令处理程序代码
	trans.ReflectZOX();
	Invalidate(FALSE);

}


void CTestView::OnRyaxis()
{
	// TODO: 在此添加命令处理程序代码
	trans.RotateY(30,P[0]);
	Invalidate(FALSE);
}


void CTestView::OnRyoz()
{
	// TODO: 在此添加命令处理程序代码
	trans.ReflectYOZ();
	Invalidate(FALSE);
}


void CTestView::OnRzaxis()
{
	// TODO: 在此添加命令处理程序代码
	trans.RotateZ(30,P[0]);
	Invalidate(FALSE);
}
