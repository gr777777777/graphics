
// TestView.cpp : CTestView 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "Test.h"
#endif

#include "TestDoc.h"
#include "TestView.h"
#include "math.h"//包含数学头文件
#define  PI 3.1415926//PI的宏定义
#define Round(d) int(floor(d+0.5))//四舍五入宏定义
#define LEFT   1   //代表:0001
#define RIGHT  2   //代表:0010
#define BOTTOM 4   //代表:0100
#define TOP    8   //代表:1000
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CTestView

IMPLEMENT_DYNCREATE(CTestView, CView)

BEGIN_MESSAGE_MAP(CTestView, CView)
	// 标准打印命令
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_COMMAND(IDM_DRAWPIC, &CTestView::OnDrawpic)
	ON_WM_LBUTTONDOWN()
//	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_COMMAND(IDM_CLIP, &CTestView::OnClip)
	ON_UPDATE_COMMAND_UI(IDM_CLIP, &CTestView::OnUpdateClip)
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

// CTestView 构造/析构

CTestView::CTestView()
{
	// TODO: 在此处添加构造代码
	PtCount=0;
	bDrawRect=FALSE;
	bClip=FALSE;
}

CTestView::~CTestView()
{
}

BOOL CTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CView::PreCreateWindow(cs);
}

// CTestView 绘制

void CTestView::OnDraw(CDC* pDC)
{
	CTestDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 在此处为本机数据添加绘制代码
	DoubleBuffer(pDC);
}


// CTestView 打印

BOOL CTestView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 默认准备
	return DoPreparePrinting(pInfo);
}

void CTestView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加额外的打印前进行的初始化过程
}

void CTestView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加打印后进行的清理过程
}


// CTestView 诊断

#ifdef _DEBUG
void CTestView::AssertValid() const
{
	CView::AssertValid();
}

void CTestView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTestDoc* CTestView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTestDoc)));
	return (CTestDoc*)m_pDocument;
}
#endif //_DEBUG


// CTestView 消息处理程序


void CTestView::OnDrawpic() 
{
	// TODO: Add your command handler code here
	PtCount=0;
	bClip=FALSE;
	bDrawRect=TRUE;
	Invalidate(FALSE);	
	MessageBox(CString("鼠标画线，剪刀裁剪"),CString("提示"),MB_OKCANCEL);
	Invalidate(FALSE);
}

void CTestView::DoubleBuffer(CDC *pDC)//双缓冲
{
	CRect rect;//定义客户区
	GetClientRect(&rect);//获得客户区的大小
	pDC->SetMapMode(MM_ANISOTROPIC);//pDC自定义坐标系
	pDC->SetWindowExt(rect.Width(),rect.Height());//设置窗口范围
	pDC->SetViewportExt(rect.Width(),-rect.Height());//设置视区范围,x轴水平向右，y轴垂直向上
	pDC->SetViewportOrg(rect.Width()/2,rect.Height()/2);//客户区中心为原点
	CDC memDC;//内存DC
	CBitmap NewBitmap,*pOldBitmap;//内存中承载的临时位图
	memDC.CreateCompatibleDC(pDC);//创建一个与显示pDC兼容的内存memDC 
	NewBitmap.CreateCompatibleBitmap(pDC,rect.Width(),rect.Height());//创建兼容位图 
	pOldBitmap=memDC.SelectObject(&NewBitmap);//将兼容位图选入memDC 
	memDC.FillSolidRect(rect,pDC->GetBkColor());//按原来背景填充客户区，否则是黑色
	memDC.SetMapMode(MM_ANISOTROPIC);//memDC自定义坐标系
	memDC.SetWindowExt(rect.Width(),rect.Height());
	memDC.SetViewportExt(rect.Width(),-rect.Height());
	memDC.SetViewportOrg(rect.Width()/2,rect.Height()/2);
	rect.OffsetRect(-rect.Width()/2,-rect.Height()/2);
	if(PtCount && !bClip)
		DrawWindowRect(&memDC);//绘制窗口
	Diamond(&memDC);
	pDC->BitBlt(rect.left,rect.top,rect.Width(),rect.Height(),&memDC,-rect.Width()/2,-rect.Height()/2,SRCCOPY);//将内存memDC中的位图拷贝到显示pDC中
	memDC.SelectObject(pOldBitmap);//恢复位图
	NewBitmap.DeleteObject();//删除位图
}
void CTestView::Diamond(CDC *pDC)
{
	double thta;//thta为圆的等分角
	int n=30;//定义等分点个数
	V=new CP2[n];
	double r=300;//定义圆的半径
	thta=2*PI/n;
	for(int i=0;i<n;i++)//计算等分点坐标
	{
		V[i].x=r*cos(i*thta);
		V[i].y=r*sin(i*thta);
	}
	for(int i=0;i<=n-2;i++)//依次各连接等分点
	{
		for(int j=i+1;j<=n-1;j++)
		{
			if(!bClip)
			{
				pDC->MoveTo(Round(V[i].x),Round(V[i].y));
				pDC->LineTo(Round(V[j].x),Round(V[j].y));
			}
			else
			{		
				P[0]=V[i];P[1]=V[j];//对金刚石的每段直线进行裁剪
				if(Cohen())					
				{
					pDC->MoveTo(Round(P[0].x),Round(P[0].y));
					pDC->LineTo(Round(P[1].x),Round(P[1].y));
					
				}
			}	
		}
	}
	delete []V;
}
CP2 CTestView::Convert(CPoint point)//坐标系变换 
{
	CRect rect;
	GetClientRect(&rect);
	CP2 ptemp;
	ptemp.x=point.x-rect.Width()/2;
	ptemp.y=rect.Height()/2-point.y;
	return ptemp;
}
void CTestView::DrawWindowRect(CDC* pDC)//绘制裁剪窗口
{
	// TODO: Add your message handler code here and/or call default
	CPen NewPen3,*pOldPen3;//定义3个像素宽度的画笔
	NewPen3.CreatePen(PS_SOLID,3,RGB(0,128,0));
	pOldPen3=pDC->SelectObject(&NewPen3);
    pDC->Rectangle(Round(Rect[0].x),Round(Rect[0].y),Round(Rect[1].x),Round(Rect[1].y));
	pDC->SelectObject(pOldPen3);
	NewPen3.DeleteObject();
}
void CTestView::EnCode(CP2 &pt)//端点编码函数
{
	pt.rc=0;
	if(pt.x<Rect[0].x)
	{
		pt.rc=pt.rc|LEFT;
	}
	else if(pt.x>Rect[1].x)
	{
		pt.rc=pt.rc|RIGHT;
	}
	if(pt.y<Rect[1].y)
	{
		pt.rc=pt.rc|BOTTOM;
	}
	else if(pt.y>Rect[0].y)
	{
		pt.rc=pt.rc|TOP;
	}
}
void CTestView::MidClip(CP2 p0,CP2 p1)//中点分割算法
{
	CP2 p;//中点坐标	
	p.x=(p0.x+p1.x)/2;p.y=(p0.y+p1.y)/2;EnCode(p);
	while(fabs(p.x-p0.x)>1e-6||fabs(p.y-p0.y)>1e-6)//判断结束
	{
		if(0==p.rc)//中点也在窗口内，则舍弃P1点
			p1=p;
		else//否则舍弃P0点
			p0=p;
		p.x=(p0.x+p1.x)/2;p.y=(p0.y+p1.y)/2;EnCode(p);
	}
	P[0]=p;
}
BOOL CTestView::Cohen()//中点分割算法
{
	EnCode(P[0]);//起点编码
	EnCode(P[1]);//终点编码
	while(P[0].rc!=0 || P[1].rc!=0)//处理至少一个顶点在窗口之外的情况
	{
		if((P[0].rc & P[1].rc)!=0)//简弃之
		{
			PtCount=0;
			return FALSE;
		}
		if(0==P[0].rc)//确保P[0]位于窗口之外
		{
			CP2 Temp;
			Temp=P[0];
			P[0]=P[1];
			P[1]=Temp;
		}
		MidClip(P[0],P[1]);
	}
	return TRUE;
}
void CTestView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	if(bDrawRect)
	{	if(PtCount<2)
		{
			Rect[PtCount]=Convert(point);
			PtCount++;
		}		
	}
	CView::OnLButtonDown(nFlags, point);
}


void CTestView::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if(!bClip)
	{
		if(PtCount<2)
		{   
			Rect[PtCount]=Convert(point);
			Invalidate(FALSE);
		}
	}
	CView::OnMouseMove(nFlags, point);
}


void CTestView::OnClip()
{
	// TODO: 在此添加命令处理程序代码
	bClip=TRUE;
	Invalidate(FALSE);
}


void CTestView::OnUpdateClip(CCmdUI *pCmdUI)
{
	// TODO: 在此添加命令更新用户界面处理程序代码
	pCmdUI->Enable((PtCount>=2)?TRUE:FALSE);
}


BOOL CTestView::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	return TRUE;
	//return CView::OnEraseBkgnd(pDC);
}
