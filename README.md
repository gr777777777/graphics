# 计算机图形学

计算机图形学基础教程(VC++)第二版，计算机图形学实践教程(VC++)第二版 孔令德

![book.jpg](./img/book.jpg)

- 课程资料地址： [https://gitlab.com/zwdcdu/graphics](https://gitlab.com/zwdcdu/graphics)

## 学习工具

- VC++6.0：原始编程工具
- qt 5.15.2：高级编程工具
- git协议：用于提交实验报告
- git账号：每个同学必须有一个gitlab账号，并上传SSH公钥。
- VSCode: 实验报告编写

## 实验准备

- 每个同学必须有一个gitlab账号，并上传SSH公钥。
- 每4到5人一个组，每个组一个组长，由组长创建一个私有项目，项目名称:12班必须是graphics_12，34班必须是graphics_34，两个班名称不同以示区别。
- 由组长将组员加入到项目中，成员的角色是maintainer
- 由组长将老师也加入到项目中。老师的账号是zwdcdu，将老师的角色也设置为maintainer。
- 在项目中创建5个文件夹：test1,test2,test3,test4,test5。表示5次实验。每次实验的源代码以及说明文件放在各自的文件夹中。
- 每个目录中必须有一个文件README.md。注意大写小写。
- 在graphics_12或者graphics_34根目录中README.md文件的内容是班级，用表格的形式写上项目成员的学号，姓名，git账号，介绍等信息。
- 在5个test目录中的README.md文件是主要的实验报告，说明每个实验的主要内容以及完成人员。除了README.md文件外，还必须有其他辅助文件。README.md文件通过链接，引用其他文件。
- 实验环境：文档编写工具用vscode或者其他支持git的工具。
- 实验环境：markdown,以及UML语言工具plantuml,mermaid等工具和标准书写文字和绘制图形，尽量不要使用word,excel,photoshop等工具。
- 每次实验中每个项目成员都必须有单独的提交痕迹，不能只是由一个同学提交，以区分团队成员的分工和工作量。原则上团队成员只维护自己的文件。
- 由于老师也是你项目中的成员，所以老师能看见你的项目并批阅，评估每个同学的工作量。
- 本项目是私有项目，因此同班的其他团队的人员是看不见的。
- 如果vscode中不能预览图像，要安装graphviz，下载[graphviz-2.38.msi](./graphviz-2.38.msi)。
- 在windows系统中安装git环境工具。下载[gitgfb_ttrar.rar](./gitgfb_ttrar.rar)

## 重要

- 每次提交时，必须写上姓名和提交原因，如“张三做了某工作”。让老师知道是你做的。计算你的工作量。也就是说每个test目录中的文件数量应该多于成员数量。但统管文件只有一个，即README.md。

## 首次实验-克隆项目

```sh
git clone 项目地址
```

## 再次实验

- git pull拉取其他成员的修改，可以通过vscode的界面，也可以通过git pull命令
  - 命令行方法
    - 进入项目目录，打开git bash
    - 运行git pull

- 通过vscode编辑，编写自己的代码/文档，然后转到git提交自己的修改部分。
- git提交可以通过vscode的界面，也可以通过命令行，方法是进入项目目录，打开git bash，运行如下命令

    ```sh
    git add .
    git commit -m '张三，做了什么修改...'
    git push
    git pull
    ```

## 实验

### 实验1

- 实验名称：反走样直线段绘制的程序设计
- 实验软件：VC++6.0
- 实验目的：使用Wu反走样算法绘制。要求用鼠标拖动绘制多条各个方向的反走样直线，并能持久显示，即屏幕刷新后仍然能显示直线。提示：可以使用VC++中的CPtrList类动态保存。另外，需要在OnDraw(CDC* pDC)函数中绘制才能持久显示。
- 实验分析：对比走样与反走样两个图形的效果，分析优点和缺点，并说明原因。
- 参见样例程序：案例5-直线反走样Wu算法，CPtrList参见： https://www.cnblogs.com/rushoooooo/archive/2011/07/24/2115429.html
- 最迟提交时间：2022-10-4

### 实验2

- 实验名称：带孔多边形填充的程序设计
- 实验软件：VC++6.0
- 实验目的：使用有效边表填充算法填充一个内部至少有两个孔的多边形，源码中不得使用高级语言自带的填充函数。
- 实验分析：程序的难点是如何设计带孔多边形的数据结构。填充以后，边缘的锯齿状非常明显，如何消除？
- 参见样例程序：案例6-多边形有效边表填充算法
- 最迟提交时间：2022-10-11

## 实验3

- 实验名称：多边形二维变换及裁剪程序设计
- 实验软件：VC++6.0
- 实验目的：以案例11为基础，修改案例11中的矩形图案为更复杂一点的多边形，并将案例14中的放大镜功能融入到案例11中。
- 实验分析：使用Liang-Barsky裁剪算法以及窗口与视口的变换
- 样例程序参见：案例11－二维图形几何变换算法,案例14-Liang-Barsky直线段裁剪算法
- 最迟提交时间：2022-11-08

## 本书常用的自定义坐标系

- 视口坐标view，X向右，Y向下，在屏幕客户矩形区域的左上角，单位是像素。
- 窗口坐标window，方向自定义，单位也是自定义。本样例是：窗口坐标原点始终在屏幕中心，X轴向右，Y轴向上，与视口的比例是1:1。
- pDC对象绘制的时候采用的是**窗口坐标系**。默认情况窗口坐标和视口坐标相同。

```c
//自定义坐标系
CRect rect;
GetClientRect(&rect);
pDC->SetMapMode(MM_ANISOTROPIC);//设置映射模式
//改变视口坐标系
pDC->SetWindowExt(rect.Width(),rect.Height());//设置窗口
pDC->SetViewportExt(rect.Width(),-rect.Height());//设置视区:x轴水平向右，y轴垂直向上
//客户区中心为坐标系原点，rect.Width()/2,rect.Height()/2坐标是设备坐标系的。
pDC->SetViewportOrg(rect.Width()/2,rect.Height()/2);
```

![book.jpg](./img/user_coord.png)

## 参考

- [教材第一版本电子:书计算机图形学基础教程.pdf](./计算机图形学基础教程.pdf)
- 目录
  - “课程源码”目录，内置教材的所有源码
  - “实践教程源码”目录，内置教材的所有源码
  - PPT，内置教材PPT
  - vc_sample: VC高级样例
    - ![VC高级样例](./img/vc_sample.png)
  - qt_sample: QT高级样例
    - ![QT高级样例](./img/qt_sample.png)
- 开发工具
  - vc6_cn_full.rar是VC++6.0
  - qt-unified-windows-x86-4.0.1-online.exe是QT的安装程序。
- VC样例使用方法
  - 打开Visual C++ 6.0
  - 执行菜单“打开工作空间”
  - 选择工作空间文件（*.dsw）。VC自动打开包含的所有文件。
  - 打开后点击执行按钮（或者F5快捷键）。
- QT样例使用方法
  - 打开Qt
  - 执行菜单“打开文件或项目”
  - 选择*.pro文件。Qt自动打开包含的所有文件。
  - 执行菜单“构建->运行”（或者CTRL+R快捷键）
- 其他参考
- [VC控件与变量绑定小结](https://www.cnblogs.com/baogg/articles/1979299.html)
- [C++代码实现光线追踪-算法](http://www.kevinbeason.com/smallpt/)
- [MFC绘制犹他茶壶](https://blog.csdn.net/ha0ha0xuexi/article/details/117375424)
- [计算机图形学之Utah茶壶光照模型](https://www.bilibili.com/video/av88365991)
- [图形学视频（共11讲）](https://www.bilibili.com/video/BV1t7411J7dV?spm_id_from=333.999.0.0)
