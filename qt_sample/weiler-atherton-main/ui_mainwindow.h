/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *pBtnCloseLoop;
    QPushButton *pBtnFinish;
    QPushButton *pBtnClip;
    QPushButton *pBtn_rtn;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QTextBrowser *label;
    QTextBrowser *txt_1;
    QPushButton *pBtnConfirm_1;
    QTextBrowser *txt_2;
    QPushButton *pBtn_confirm_2;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(800, 600);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        horizontalLayoutWidget = new QWidget(centralwidget);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(0, 0, 801, 28));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        pBtnCloseLoop = new QPushButton(horizontalLayoutWidget);
        pBtnCloseLoop->setObjectName(QString::fromUtf8("pBtnCloseLoop"));

        horizontalLayout->addWidget(pBtnCloseLoop);

        pBtnFinish = new QPushButton(horizontalLayoutWidget);
        pBtnFinish->setObjectName(QString::fromUtf8("pBtnFinish"));

        horizontalLayout->addWidget(pBtnFinish);

        pBtnClip = new QPushButton(horizontalLayoutWidget);
        pBtnClip->setObjectName(QString::fromUtf8("pBtnClip"));

        horizontalLayout->addWidget(pBtnClip);

        pBtn_rtn = new QPushButton(horizontalLayoutWidget);
        pBtn_rtn->setObjectName(QString::fromUtf8("pBtn_rtn"));

        horizontalLayout->addWidget(pBtn_rtn);

        verticalLayoutWidget = new QWidget(centralwidget);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(550, 40, 241, 511));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label = new QTextBrowser(verticalLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAutoFillBackground(false);

        verticalLayout->addWidget(label);

        txt_1 = new QTextBrowser(verticalLayoutWidget);
        txt_1->setObjectName(QString::fromUtf8("txt_1"));

        verticalLayout->addWidget(txt_1);

        pBtnConfirm_1 = new QPushButton(verticalLayoutWidget);
        pBtnConfirm_1->setObjectName(QString::fromUtf8("pBtnConfirm_1"));

        verticalLayout->addWidget(pBtnConfirm_1);

        txt_2 = new QTextBrowser(verticalLayoutWidget);
        txt_2->setObjectName(QString::fromUtf8("txt_2"));

        verticalLayout->addWidget(txt_2);

        pBtn_confirm_2 = new QPushButton(verticalLayoutWidget);
        pBtn_confirm_2->setObjectName(QString::fromUtf8("pBtn_confirm_2"));

        verticalLayout->addWidget(pBtn_confirm_2);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 22));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        pBtnCloseLoop->setText(QCoreApplication::translate("MainWindow", "\351\227\255\345\220\210\345\244\232\350\276\271\345\275\242", nullptr));
        pBtnFinish->setText(QCoreApplication::translate("MainWindow", "\345\256\214\346\210\220\345\275\223\345\211\215\345\244\232\350\276\271\345\275\242\347\273\230\347\224\273", nullptr));
        pBtnClip->setText(QCoreApplication::translate("MainWindow", "\350\243\201\345\211\252", nullptr));
        pBtn_rtn->setText(QCoreApplication::translate("MainWindow", "\350\277\224\345\233\236\350\243\201\345\211\252", nullptr));
        label->setHtml(QCoreApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'SimSun'; font-size:13.5pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'SimSun'; font-size:9pt;\">\344\270\273\345\244\232\350\276\271\345\275\242\344\270\272\347\272\242\350\211\262.</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'SimSun'; font-size:9pt;\">\350\243\201\345\211\252\345\244\232\350\276\271\345\275\242\344\270\272\351\273\221\350\211\262\343\200\202</span></p></body></html>", nullptr));
        txt_1->setHtml(QCoreApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'SimSun'; font-size:13.5pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'SimSun'; font-size:9pt;\">1\345\217\267\345\244\232\350\276\271\345\275\242</span></p></body></html>", nullptr));
        pBtnConfirm_1->setText(QCoreApplication::translate("MainWindow", "\350\256\276\347\275\2561\345\217\267\344\270\272\344\270\273\345\244\232\350\276\271\345\275\242", nullptr));
        txt_2->setHtml(QCoreApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'SimSun'; font-size:13.5pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'SimSun'; font-size:9pt;\">2\345\217\267\345\244\232\350\276\271\345\275\242</span></p></body></html>", nullptr));
        pBtn_confirm_2->setText(QCoreApplication::translate("MainWindow", "\350\256\276\347\275\2562\345\217\267\344\270\272\344\270\273\345\244\232\350\276\271\345\275\242", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
