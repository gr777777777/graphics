//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by FillDib.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_FILLDITYPE                  129
#define IDD_INFODLG                     130
#define IDC_INFO                        1000
#define IDD_SETCOLOR                    32771
#define IDD_SHOWINFO                    32773

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32775
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
