// TaskDoc.cpp : implementation of the CTaskDoc class
//

#include "stdafx.h"
#include "Task.h"

#include "TaskDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTaskDoc

IMPLEMENT_DYNCREATE(CTaskDoc, CDocument)

BEGIN_MESSAGE_MAP(CTaskDoc, CDocument)
	//{{AFX_MSG_MAP(CTaskDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTaskDoc construction/destruction

CTaskDoc::CTaskDoc()
{
	// TODO: add one-time construction code here

}

CTaskDoc::~CTaskDoc()
{
}

BOOL CTaskDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CTaskDoc serialization

void CTaskDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CTaskDoc diagnostics

#ifdef _DEBUG
void CTaskDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CTaskDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTaskDoc commands
