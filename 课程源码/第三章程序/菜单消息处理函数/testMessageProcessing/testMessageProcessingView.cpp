// testMessageProcessingView.cpp : implementation of the CTestMessageProcessingView class
//

#include "stdafx.h"
#include "testMessageProcessing.h"

#include "testMessageProcessingDoc.h"
#include "testMessageProcessingView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestMessageProcessingView

IMPLEMENT_DYNCREATE(CTestMessageProcessingView, CView)

BEGIN_MESSAGE_MAP(CTestMessageProcessingView, CView)
	//{{AFX_MSG_MAP(CTestMessageProcessingView)
	ON_COMMAND(ID_MDRAW, OnMdraw)
//	ON_COMMAND(ID_topMenu, OntopMenu)
	ON_COMMAND(ID_PopMenu, OnNewPopMenu)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestMessageProcessingView construction/destruction

CTestMessageProcessingView::CTestMessageProcessingView()
{
	// TODO: add construction code here

}

CTestMessageProcessingView::~CTestMessageProcessingView()
{
}

BOOL CTestMessageProcessingView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CTestMessageProcessingView drawing

void CTestMessageProcessingView::OnDraw(CDC* pDC)
{
	CTestMessageProcessingDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here

//	OnMdraw();
}

/////////////////////////////////////////////////////////////////////////////
// CTestMessageProcessingView printing

BOOL CTestMessageProcessingView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CTestMessageProcessingView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CTestMessageProcessingView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CTestMessageProcessingView diagnostics

#ifdef _DEBUG
void CTestMessageProcessingView::AssertValid() const
{
	CView::AssertValid();
}

void CTestMessageProcessingView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTestMessageProcessingDoc* CTestMessageProcessingView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTestMessageProcessingDoc)));
	return (CTestMessageProcessingDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTestMessageProcessingView message handlers

void CTestMessageProcessingView::OnMdraw() 
{
	// TODO: Add your command handler code here
     RedrawWindow(); //重新绘制窗口，将以前的图形清除


	CDC *pDC = GetDC();

	CRect rect;                                         //定义矩形
	GetClientRect(&rect);                               //获得客户区矩形的大小
	pDC->SetMapMode(MM_ANISOTROPIC);                    //自定义坐标系
	pDC->SetWindowExt(rect.Width(),rect.Height());      //设置窗口比例
	pDC->SetViewportExt(rect.Width(),-rect.Height());   //设置视区比例，且x轴水平向右，y轴垂直向上
	pDC->SetViewportOrg(rect.Width()/2,rect.Height()/2);//设置客户区中心为坐标系原点
	rect.OffsetRect(-rect.Width()/2,-rect.Height()/2);  //矩形与客户区重合

	int R=200;
	MBCircle(R,pDC);


	ReleaseDC(pDC);
	
}

void CTestMessageProcessingView::MBCircle(int R,CDC *pDC)//圆中点Bresenham算法
{
	int x,y;
	double d;	 
	d=1.25-R;x=0;y=R;
	for(x=0;x<=y;x++)
	{
		CirclePoint(x,y,pDC);//调用八分法画圆子函数
        if (d<0)
			d+=2*x+3;
        else
		{
			d+=2*(x-y)+5;
			y--;
		} 
     }
}

void CTestMessageProcessingView::CirclePoint(int x, int y,CDC *pDC)//八分法画圆子函数
{
	COLORREF  clr=RGB(0,0,255);                         //定义圆的边界颜色
	pDC->SetPixelV(x,y,clr);     //x,y
	pDC->SetPixelV(y,x,clr);     //y,x
	pDC->SetPixelV(y,-x,clr);    //y,-x
	pDC->SetPixelV(x,-y,clr);    //x,-y
	pDC->SetPixelV(-x,-y,clr);   //-x,-y
	pDC->SetPixelV(-y,-x,clr);   //-y,-x
	pDC->SetPixelV(-y,x,clr);    //-y,x
	pDC->SetPixelV(-x,y,clr);    //-x,y
}



void CTestMessageProcessingView::OntopMenu() 
{
	// TODO: Add your command handler code here
    RedrawWindow(); //重新绘制窗口，将以前的图形清除

	CDC *pDC = GetDC();

	CRect rect;                                         //定义矩形
	GetClientRect(&rect);                               //获得客户区矩形的大小
	pDC->SetMapMode(MM_ANISOTROPIC);                    //自定义坐标系
	pDC->SetWindowExt(rect.Width(),rect.Height());      //设置窗口比例
	pDC->SetViewportExt(rect.Width(),-rect.Height());   //设置视区比例，且x轴水平向右，y轴垂直向上
	pDC->SetViewportOrg(rect.Width()/2,rect.Height()/2);//设置客户区中心为坐标系原点
	rect.OffsetRect(-rect.Width()/2,-rect.Height()/2);  //矩形与客户区重合

	int R=20;
	MBCircle(R,pDC);


	ReleaseDC(pDC);
	
}

void CTestMessageProcessingView::OnNewPopMenu() 
{
	// TODO: Add your command handler code here
    RedrawWindow(); //重新绘制窗口，将以前的图形清除

	CDC *pDC = GetDC();

	CRect rect;                                         //定义矩形
	GetClientRect(&rect);                               //获得客户区矩形的大小
	pDC->SetMapMode(MM_ANISOTROPIC);                    //自定义坐标系
	pDC->SetWindowExt(rect.Width(),rect.Height());      //设置窗口比例
	pDC->SetViewportExt(rect.Width(),-rect.Height());   //设置视区比例，且x轴水平向右，y轴垂直向上
	pDC->SetViewportOrg(rect.Width()/2,rect.Height()/2);//设置客户区中心为坐标系原点
	rect.OffsetRect(-rect.Width()/2,-rect.Height()/2);  //矩形与客户区重合

	int R=20;
	MBCircle(R,pDC);


	ReleaseDC(pDC);

	
}
