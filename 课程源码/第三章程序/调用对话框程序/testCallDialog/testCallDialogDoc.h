// testCallDialogDoc.h : interface of the CTestCallDialogDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_TESTCALLDIALOGDOC_H__FD29F0AC_D359_4055_9235_F69B6EC5C388__INCLUDED_)
#define AFX_TESTCALLDIALOGDOC_H__FD29F0AC_D359_4055_9235_F69B6EC5C388__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CTestCallDialogDoc : public CDocument
{
protected: // create from serialization only
	CTestCallDialogDoc();
	DECLARE_DYNCREATE(CTestCallDialogDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestCallDialogDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTestCallDialogDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CTestCallDialogDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTCALLDIALOGDOC_H__FD29F0AC_D359_4055_9235_F69B6EC5C388__INCLUDED_)
