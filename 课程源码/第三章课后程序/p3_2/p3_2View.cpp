// p3_2View.cpp : implementation of the CP3_2View class
//

#include "stdafx.h"
#include "p3_2.h"
#include "Line.h"
#include "P2.h"
#include "p3_2Doc.h"
#include "p3_2View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CP3_2View

IMPLEMENT_DYNCREATE(CP3_2View, CView)

BEGIN_MESSAGE_MAP(CP3_2View, CView)
	//{{AFX_MSG_MAP(CP3_2View)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CP3_2View construction/destruction

CP3_2View::CP3_2View()
{
	// TODO: add construction code here

}

CP3_2View::~CP3_2View()
{
}

BOOL CP3_2View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CP3_2View drawing

void CP3_2View::OnDraw(CDC* pDC)
{
	CP3_2Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
	CRect rect;
	GetClientRect(&rect);
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetWindowExt(rect.Width(),rect.Height());
	pDC->SetViewportExt(rect.Width(),-rect.Height());
	pDC->SetViewportOrg(rect.Width()/2,rect.Height()/2);
	rect.OffsetRect(-rect.Width()/2,-rect.Height()/2);
	
	CP2 p0(0.0,0.0), p1(600.0,300.0);
	CLine line;
	line.MoveTo(pDC,p0);
	line.LineTo(pDC,p1);
	CString s;
    s.Format("%s %2lf","直线的斜率为：",line.k);
	MessageBox(s,"温馨提示");


}

/////////////////////////////////////////////////////////////////////////////
// CP3_2View printing

BOOL CP3_2View::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CP3_2View::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CP3_2View::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CP3_2View diagnostics

#ifdef _DEBUG
void CP3_2View::AssertValid() const
{
	CView::AssertValid();
}

void CP3_2View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CP3_2Doc* CP3_2View::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CP3_2Doc)));
	return (CP3_2Doc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CP3_2View message handlers
