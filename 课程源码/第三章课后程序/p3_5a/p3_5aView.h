// p3_5aView.h : interface of the CP3_5aView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_P3_5AVIEW_H__86D10D19_5025_485B_8B22_7E590C6E39AA__INCLUDED_)
#define AFX_P3_5AVIEW_H__86D10D19_5025_485B_8B22_7E590C6E39AA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CP3_5aView : public CView
{
protected: // create from serialization only
	CP3_5aView();
	DECLARE_DYNCREATE(CP3_5aView)

// Attributes
public:
	CP3_5aDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CP3_5aView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CP3_5aView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CP3_5aView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in p3_5aView.cpp
inline CP3_5aDoc* CP3_5aView::GetDocument()
   { return (CP3_5aDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_P3_5AVIEW_H__86D10D19_5025_485B_8B22_7E590C6E39AA__INCLUDED_)
